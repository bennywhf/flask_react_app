# Getting Started
~~~~    
cd frontend
npm install
cd ..
python api.py
~~~~
## Resources
* https://reactjs.org/tutorial/tutorial.html#setup-option-1-write-code-in-the-browser --Make sure to skip any installation instructions.
* Get familiar with SPAs (Single Page Application) https://en.wikipedia.org/wiki/Single-page_application
* build a simple API using flask https://www.youtube.com/watch?v=s_ht4AKnWZg
* turn that API into your backend for your Single Page Application