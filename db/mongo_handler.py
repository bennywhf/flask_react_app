from pymongo import MongoClient


# The two values variables below are the mongo host adresss (IP address or domain name) and the port.
MONGO_HOST = "localhost"
PORT = 27017


class MongoHandler(object):
    def __init__(self):
        self.client = MongoClient(MONGO_HOST, PORT)

    def get_collection(self, db, collection):
        return self.client[db][collection]
