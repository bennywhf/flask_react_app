from flask import Flask, request, render_template, send_from_directory
import os


app = Flask(__name__, template_folder='frontend')
root_dir = os.path.dirname(os.path.realpath(__file__))


@app.route('/')
def home():
    return render_template('index.html')


@app.route('/js/<path:file_path>')
def get_static_file(file_path):
    return send_from_directory(os.path.join(root_dir, 'frontend', 'node_modules'), file_path)


@app.route('/dist/<path:file_path>')
def get_dist_file(file_path):
    return send_from_directory(os.path.join(root_dir, 'frontend', 'dist'), file_path)


if __name__ == '__main__':
    app.run()
