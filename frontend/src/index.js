import React from 'react';
import ReactDOM from 'react-dom';
import MainAppBar from './app-bar/MainAppBar.jsx'
import CenteredGrid from './app-grid/CenteredGrid.jsx'

class Welcome extends React.Component {
  render() {
    return (<div style={{marginLeft:'5%', marginRight: '5%'}}>
            <MainAppBar />
            <h1 style={{textAlign: 'center'}}>Student Loan Info (Under Construction...)</h1>
            <CenteredGrid />
            </div>);
  }
}

ReactDOM.render(<Welcome />, document.getElementById('root'));