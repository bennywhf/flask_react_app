import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
    height: '250px'
  },
  imageholder: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
    height: '400px',
    backgroundColor: 'gray'
  },
});

function CenteredGrid(props) {
  const { classes } = props;

  return (
    <div className={classes.root}>
      <Grid container spacing={24}>
        <Grid item xs={4}>
          <Paper className={classes.paper}></Paper>
        </Grid>
        <Grid item xs={4}>
          <Paper className={classes.paper}></Paper>
        </Grid>
        <Grid item xs={4}>
          <Paper className={classes.paper}></Paper>
        </Grid>
        <Grid item xs={12}>
        </Grid>
        <Grid item xs={6}>
          <Paper className={classes.imageholder}>Image here.</Paper>
        </Grid>
        <Grid item xs={6}>
        <div style={{fontSize: '20px'}}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sem nibh, tempor et scelerisque vehicula, cursus vitae mi. Integer vitae ultricies ante. Sed pharetra magna eu magna lobortis rhoncus. Donec elit mi, venenatis ut orci non, gravida congue sapien. In tincidunt orci a vestibulum tincidunt. Nam ac risus ac dui scelerisque tincidunt. Nunc euismod malesuada ex sed hendrerit.
        </div>
        <p></p>
        <div style={{fontSize: '20px'}}>
            Vivamus orci ante, aliquet nec interdum sit amet, convallis nec nibh. Praesent aliquet purus in orci venenatis tristique. Nam sagittis leo sed hendrerit volutpat. Etiam lobortis justo enim, non ultricies dolor congue id. Aliquam viverra, lorem sit amet congue dignissim, tortor sapien convallis nisi, condimentum auctor nulla metus nec nisl. Maecenas ut vulputate nisl, et pellentesque lectus. Donec malesuada, urna vel laoreet malesuada, risus nisl ornare quam, vel faucibus elit orci nec ante. Sed fermentum nisi ut dignissim congue.
        </div>
        </Grid>
      </Grid>
    </div>
  );
}

CenteredGrid.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CenteredGrid);